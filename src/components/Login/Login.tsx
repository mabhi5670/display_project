import { useForm } from "react-hook-form";
import {yupResolver} from '@hookform/resolvers/yup'
import * as yup from 'yup';
import styles from './Login.module.scss'


import { useHistory } from "react-router";
import {IForm} from './Login';
const schema = yup.object().shape({
    username: yup.string().required('Please Enter your email').matches(/^[^\s@]+@[^\s@]+\.[^\s@]+$/),
       
    password: yup
 .string()
 .required('Please Enter your password')
 .matches(
   /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
   "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character"
 ),

})
const Login= () => {

   const history = useHistory();
   const { register,  handleSubmit, formState: { errors } } = useForm<IForm>({
    resolver:yupResolver(schema),
   });
    

    const onSubmit = (data: IForm) => console.log(data);

    return(
        
         <div className={styles.main}>
         <div className={styles.text}>
         <div className={styles.text1}>Something</div>
         <div className={styles.text2}>another text</div>
         <div className={styles.box}>
         <form className={styles.inputFields} onSubmit={handleSubmit(onSubmit)}>
         <label>
          <p>Username</p>
          </label>
         
         <div className="container">
      <input
        type="email"
        placeholder="Enter your email"
        className="email-input"
        {...register}
      />
    </div>
          
        <label>
          <p>Password</p>
          </label>
          <input
        type="password"
        placeholder="Enter your password"
        className="password-input"
        {...register}
      />

        
        <div className={styles.btn}>
          <button type="submit" className={styles.btn1}>Submit</button>
        </div>
        </form>
        </div>
        </div>
        </div>
        
    )

}

export default Login;
