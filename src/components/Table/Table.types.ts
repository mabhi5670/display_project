export interface Data {
  Operation: number;
  Make: number;
  Tool_Type: number;
    name: string;
    Sub_Tool_Type: number;
  }

  export interface HeadCell {
    disablePadding: boolean;
    id: keyof Data;
    label: string;
    numeric: boolean;
  }
  export type Order = 'asc' | 'desc';
  export interface EnhancedTableProps {
    numSelected: number;
    onRequestSort: (event: React.MouseEvent<unknown>, property: keyof Data) => void;
    onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
    order: Order;
    orderBy: string;
    rowCount: number;
  }

  export interface EnhancedTableToolbarProps {
    numSelected: number;
  }