import { Box, Checkbox, TableCell, TableRow, TableSortLabel,  TableHead } from "@mui/material";

import {  Data, EnhancedTableProps, HeadCell } from "./Table.types";
import { visuallyHidden } from '@mui/utils';



const headCells: readonly HeadCell[] = [
    {
      id: 'name',
      numeric: false,
      disablePadding: true,
      label: 'Tool Name',
    },
    {
      id: 'Operation',
      numeric: true,
      disablePadding: false,
      label: 'Operation',
    },
    {
      id: 'Make',
      numeric: true,
      disablePadding: false,
      label: 'Make',
    },
    {
      id: 'Tool_Type',
      numeric: true,
      disablePadding: false,
      label: 'Tool_Type',
    },
    {
      id: 'Sub_Tool_Type',
      numeric: true,
      disablePadding: false,
      label: 'Sub_Tool_Type',
    },
  ];
  
  
  
  export default function EnhancedTableHead(props: EnhancedTableProps) {
    const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } =
      props;
    const createSortHandler =
      (property: keyof Data) => (event: React.MouseEvent<unknown>) => {
        onRequestSort(event, property);
      };
  
    return (
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              color="primary"
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={rowCount > 0 && numSelected === rowCount}
              onChange={onSelectAllClick}
              inputProps={{
                'aria-label': 'select all desserts',
              }}
            />
          </TableCell>
          {headCells.map((headCell) => (
            <TableCell
              key={headCell.id}
              align={headCell.numeric ? 'right' : 'left'}
              padding={headCell.disablePadding ? 'none' : 'normal'}
              sortDirection={orderBy === headCell.id ? order : false}
            >
              <TableSortLabel
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : 'asc'}
                onClick={createSortHandler(headCell.id)}
              >
                {headCell.label}
                {orderBy === headCell.id ? (
                  <Box component="span" sx={visuallyHidden}>
                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                  </Box>
                ) : null}
              </TableSortLabel>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
    );
  }