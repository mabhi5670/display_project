import React from 'react'
import styles from './Dashboard.module.scss';
//import {IProps} from './Dashboard.types';
import { Link } from 'react-router-dom';
import Pill from '../Pill/Pill';


const Dashboard = () => {
    return(
        <div className={styles.sidebars}>
            <nav className={styles.row}>
                <Link to="/" className={styles.row1}>
                <Pill text="Tools"/>
                </Link>
               
                <Link to="/bookings" className={styles.row2}>
                <Pill text="Bookings" />
                </Link>

                <Link to="/profile" className={styles.row3}>
                <Pill text="Profile"/>
                </Link>
            </nav>

  </div>
    
    )
}
export default Dashboard;